require 'rails_helper'

RSpec.describe Tweet, type: :model do
  describe 'validations' do
    context 'body' do
      it 'valid lenght' do
        tweet = build(:tweet, body: '12334')
        expect(tweet).to be_valid
      end

      it 'invalid lenght' do
        tweet = build(:tweet, body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took")
        expect(tweet).to_not be_valid
      end

      context 'duplicated' do
        let(:user) { create(:user) }

        it 'in the past 24h' do
          create(:tweet, body: '123', user: user)
          tweet = build(:tweet, body: '123', user: user)

          expect(tweet).to_not be_valid
        end

        it 'in more than 24h' do
          create(:tweet, body: '123', user: user, created_at: DateTime.now-2.days)
          tweet = build(:tweet, body: '123', user: user)

          expect(tweet).to be_valid
        end
      end
    end
  end
end
