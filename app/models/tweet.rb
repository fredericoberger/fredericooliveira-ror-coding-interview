# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { maximum: 180 }
  validates_uniqueness_of :body, scope: :user, conditions: -> { where(created_at: DateTime.now-24.hours..) }

  scope :unaffiliated, -> { joins(:user).where(user: { company_id: nil })}
end
